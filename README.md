This software is intended for use by everyone and is free to use and copy

	The purpose of this software is to provide a tool where a user can select a
	text file and receive information about the text. The features this tool
	includes are counting the total number of words, total number of unique
	words, and create a list with the number of times those unique words appear
	inside of a text file.

	Before running, the user must have the java virtual machine installed. This
	is necessary for the program to be able to be executed. If you do not have
	the java virtual machine installed, the below link will lead you to a
	download of the software. Once on the linked page, click "Download Java" and
	follow the ensuing instructions.

		https://www.java.com/en/

	Only this folder is needed to run the program. All files and libraries are
	included. If you have the java virtual machine installed, no other
	installation will be neccesary to access this tool.

	(Talk about how to open tool here once you have the project completed. As of
	9/21/17, there is no file to open, so there is no way to explain how to open
	the tool.)

	Upon opening, the user will simply select the file in which they would like
	to have these operations performed. A simple GUI has been programmed to make
	this task as simple as possible. The tool will complete the rest of the
	operations and return the counts, displaying them neatly for the user.

	Some things to know about how the tool does it's count:

		Unique words are counted the same reguardless of case. For example, the
		word "The" and "the" will be counted as the same word.

		The total word count will only include actual words. Punctuation of any
		kind will not count as a word.

		The count for each uniquie word will be listed next to the unique word,
		from most prolific to least prolific.



	If you have questions or suggestions, my contact information has been listed
	below.

		Matt Muller mullermm@augsburg.edu

	This software is intended for use by everyone and is free to use and copy