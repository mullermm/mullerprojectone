package wordreader;

import java.io.*;
import java.util.*;

/**
This class is used to count words in a text file, how many unique words exist in that text
file, and how many times each unique word has been used.
*/
public class WordReader{	
	
	/**
	This method will count the number of words existing in a text file.
	
	@param 	fileDirectory						holds file directory of file to be read into File class
	@throws	FileNotFoundException				exception for File class 
	*/
	public static int wordCount(File file) throws FileNotFoundException{
				
        Scanner in = new Scanner(file);						//creates a new scanner class from the file class in the previous line
		
		int count = 0;										//temp count variable
		
		while(in.hasNext()){								//this statement runs as long as there is text remaining in text file
			
			String temp = in.next();						//stores next word of text into temp
			
			temp = temp.replaceAll("[^a-zA-Z ]", " ");		//replaces all non a-z, A-Z characters with a space
			
			if(temp.length() > 0){							//if the temp (word read in from file) is actually > 0, count is incramented
			count++;
			}
		}
		
		return count;
		
	}
	
	/**
	This method is used to create a hashmap of words in a text file. The structure of the key
	value pair is set up as so: the key being hashed is a word from the text file, the value is
	the count of how many times that word appears in the text file. If the word being hashed doesnt
	yet exist in the hashmap, the word is stored as the key and the value is set to 1. If the word
	being hashed already exist in the hashmap, the key is left alone while the value is incremented.
	
	@param 	fileDirectory						holds file directory of file to be read into File class
	@return	created hashmap
	@throws	FileNotFoundException				exception for File class
	*/
	public static HashMap<String,Integer> createHashmap(File file) throws FileNotFoundException{
		
		//creates hashmap hm with generics String, Integer as key value pair
		HashMap<String,Integer> hm = new HashMap<String, Integer>();
				
        Scanner in = new Scanner(file);						//creates a new scanner class from the file class in the previous line
		
		int i = 0;											//temp count variable
		
		while(in.hasNext()){								//this statement runs as long as there is text remaining in text file					
		
		
			/*This line reads in the next token,replaces all characters ^a-z A-Z 
			with " ", converts all characters to lowercase, splits token into 
			individual strings by delim " ", and stores them into array entry */
			String[] entry = in.next().replaceAll("[^a-zA-Z]", " ").toLowerCase().split("\\s+");
		
			for(i = 0; i < entry.length; ++i){				//runs for the length of array entry
				if(hm.get(entry[i]) == null){				//if hashed entry is empty
					
					hm.put(entry[i], 1);					//place hashed word into key and set value to 1
				}
				else{
				
					hm.put(entry[i], hm.get(entry[i]) + 1);	//just incrament the value because key already exist 
				
				}
			
			}
		}
		
		
		hm.remove(""); //removes potential blank entry from a token scanned in with no letters
		
		return hm; // returns hashmap
	
	}
	
	/**
	This method is used to print the size of a hashmap. It simply gets the size of the hm and returns it as an int.
	
	@param temp 									holds hashmap being sent
	*/
	public static int hashMapSize(HashMap<String,Integer> temp){
		
		return temp.size();
	
	}
	
	/**
	This method is used to print the actual hashmap. The hashmap is printed to a text file named "UniqueWords.txt". 
	
	@param temp 									holds hashmap being sent
	@throws IOException 							exception for FileOutputStream class
	*/
	public static void printHashMapList(HashMap<String,Integer> temp) throws IOException{
		
		int hashSize = temp.size();							//takes size of hashmap sent to function and stores into hashSize		
					
		File file = new File("UniqueWords.txt");			//creates File class using file UniqueWords.txt			
			
		FileOutputStream fos = new FileOutputStream(file);	//creates FileOutputStream class from file UniqueWords
			
		PrintWriter pw = new PrintWriter(fos);				//creates PrintWiter class from FileOutputStream fos

		for(Map.Entry<String,Integer> m :temp.entrySet()){	//looks entrys inside of hashmap temp and excludes empty locations
				
			pw.println(m.getValue() + " " + m.getKey());	//prints the key value pairs of hashmap
				
		}

		pw.flush();											//following 3 lines clean up PrintWriter and FileOutputStream
		pw.close();
		fos.close();
			
		
			
	}
	
}