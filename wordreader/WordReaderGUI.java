package wordreader;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.util.*;

/**
These classes and methods are used to create a JFrame, create Jlabels and JButtons, size and name 
those JFrames and JButtons, and create an action listener inside of the JFrame. The action listener
is used to listen for a button being pressed. When that button is pressed, a file selection window
is created and a text file can then be selected. This file is then sent to WordReader.java 
and analyzed for word count, unique word count, and number of times those words are created. The word count
and unique word count are sent back to this code while a list of unique words is created in WordReader.java.
The JFrame will then display these values as well as a message explaining to the user that a unique words list
has been printed to a file

Some of this code has been copied from examples Erik Stienmetz provided
*/
public class WordReaderGUI extends JFrame{

   	//JLabel constructors
   																		//Creates and labels total words label box in GUI 
   private JLabel selectFileLabel = new JLabel( "Select A file to be counted for total word and unique words!"); 
   private JLabel printedWords = new JLabel( ""); 						//Creates and labels printed words label box in GUI  			
   private JLabel totalWords = new JLabel( "Total Words:");   			//Creates and labels total words label box in GUI 
   private JLabel totalWordsAnswer = new JLabel( "     ");   			//Creates and labels total words answer label box in GUI 
   private JLabel uniqueWords = new JLabel( "Unique Words:");   		//Creates and labels unique words label box in GUI 
   private JLabel uniqueWordsAnswer = new JLabel( "     ");   			//Creates and labels unique words answer label box in GUI 
   	
   /** 
	This main simply calls the GUI	
		
	*/
	public static void main(String[] args){

		WordReaderGUI readerGUI = new WordReaderGUI();					//Constructs new WordReaderGUI names readerGUI
	
	}
   
   
   
   	
   //Jbutton
   private JButton fileSelectButton = new JButton("Select File");		//Creates and names "Select File:" button (variable name is fileSelectButton)
   	
   /**
   The following takes the constructed JLabels and JButton and sets their size, color, and other attributes. They
   are then added to the content plane of the JFrame. At the end of this method, the JFrame has been set to visibile.
   */
    public WordReaderGUI(){
		
		//created main JFrame Window
		super("Word Reader");											//Names JFrame "Word Reader"
		this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);			//Sets program to close when GUI closes
		this.setBounds( 0, 0, 500, 350);								//Sets bounds of JFrame Width = 500 and Height = 600, starting at 0,0
		this.getContentPane().setLayout(null);							//creates layout of JFrame as blank
		
		//Select File Label
		selectFileLabel.setBounds(60, 15, 400, 20);		 				//Location and size of totalWords box
		this.getContentPane().add(selectFileLabel);						//Adds totalWords to JFrame 
		
		//File Selector Button
		fileSelectButton.setBounds(200, 60, 100, 30);					//Sizes fileSelectButton
		this.getContentPane().add(fileSelectButton);					//Adds fileSelectButton to JFrame 		
        fileSelectButton.addActionListener(new ButtonListener());  		//Creates action listener on fileSelectButton. Action is performed when button is clicked
		
		
		//Total Words Label
		totalWords.setBounds(100, 150, 100, 20);		 				//Location and size of totalWords box
		this.getContentPane().add(totalWords);							//Adds totalWords to JFrame 	
		
		//Total Words Answer Label
		totalWordsAnswer.setBounds(325, 150, 60, 20);		 			//Location and size of totalWordsAnswer box
		totalWordsAnswer.setOpaque(true);								//Makes label box opaque and not transparent
		totalWordsAnswer.setBackground(Color.lightGray);				//Changes label box color to grey
		this.getContentPane().add(totalWordsAnswer);					//Adds totalWordsAnswer to JFrame 
		
		//Unique Words Label
		uniqueWords.setBounds(100, 200, 100, 20);						//Location and size of uniqueWords box
		this.getContentPane().add(uniqueWords);							//Adds uniqueWords to JFrame
		
		//Total Words Answer Label
		uniqueWordsAnswer.setBounds(325, 200, 60, 20);		 			//Location and size of uniqueWordsAnswer box
		uniqueWordsAnswer.setOpaque(true);								//Makes label box opaque and not transparent
		uniqueWordsAnswer.setBackground(Color.lightGray);				//Changes label box color to grey
		this.getContentPane().add(uniqueWordsAnswer);					//Adds totalWordsAnswer to JFrame
		
		//File Output Printed Label									
		printedWords.setBounds(25, 260, 450, 20);						//Location and size of printedWords box
		this.getContentPane().add(printedWords);						//Adds printedWords to JFrame
		
		//Makes constructed JFrame visible
		this.setVisible(true);											//Makes JFrame Visibile
		
	}	
	/**
	This class is an action listener that is assigned to fileSelectButton. When the fileSelectButton is pressed 
	on the JFrame, this code is executed. First, a JFileChooser object is created. Then, the file filter is set. 
	The file filter is set up to only accept text files. Then, the chooserSuccess is set. An if else statement is used 
	to check this chooserSuccess value (1 if file selected, 0 if cancel was selected). If the chooserSucesss is 1,
	meaning a file has been selected from the file chooser, the selected file is saved to a file object names chosenFile. 
	This file object can then be analyzed. The processingFile() function is then called. If the chooserSuccess is not 1, the else part of the 
	if else statement is called, which will be blank.
	*/
	public class ButtonListener implements ActionListener{
		
			public void actionPerformed(ActionEvent e) { 					//Looks for when selectFileBullon has been pressed
				
            	JFileChooser chooser = new JFileChooser();					//Creates new JFileChooser Class
            																//Filters files that JFileChooser will accept
            	chooser.setFileFilter( new FileNameExtensionFilter("TEXT FILES", "txt", "text"));
            	
            	int chooserSuccess = chooser.showOpenDialog( null);			//Sets chooserSuccess to 1 if user selected file, 0 if they didnt
            	
            	if( chooserSuccess == JFileChooser.APPROVE_OPTION) {		//If user selected a file
            	
                	File chosenFile = chooser.getSelectedFile();			//Creates new File object with file selected by user
				
					processingFile(chosenFile);								//calls processingFile function to process file
                	
            	}
            	else {														//User didnt select a file
                	System.out.println("You hit cancel");					//Lets command window know user did not select a file
            	}
			}
	}
	/**
	This method is used to call multiple functions. First, the File is sent to wordCount() and the word count is returned to an integer.
	Then hashMap h is created using createHashmap(). hm is then sent to hashMapSize(), which returns the size and saves it as an int uniqueWords.
	Then printHashMapList() prints the hashmap to a text file "UniqueWords.txt". Then, JLabels printedWords, totalWordsAnswer, and uniqueWordsAnswer
	are all updated and displayed in JFrame.
	
	@param file 	This contains the File object being passed in
	*/
	public void processingFile(File file){
	
		try{
			int totalWords = WordReader.wordCount(file);					//gets the word count of selected text and prints it to console
						
			HashMap<String,Integer> hm = WordReader.createHashmap(file);	//creates a hashmap of the words in the selected text. Key = word, Value = # of times word appears in text
						
			int uniqueWords = WordReader.hashMapSize(hm);					//finds the size of the hashmap and prints it as the # of unique words in the selected text
						
			WordReader.printHashMapList(hm);								//prints the hashmap to console if size < 10, or prints to file if bigger
						
			WordReaderGUI.this.printedWords.setText("List of unique words and usage has been outputed to UniqueWords.txt");
		
			WordReaderGUI.this.totalWordsAnswer.setText(String.valueOf(totalWords));	//sets JLabel totalWordsAnswer to value calculated in WordReader
		
			WordReaderGUI.this.uniqueWordsAnswer.setText(String.valueOf(uniqueWords));	//sets JLabel uniqueWordsAnswer to value calculated in WordReader
						
		}
		catch(Exception y){										//Catches exceptions from FILE and IO
		}
		
	}
	
}